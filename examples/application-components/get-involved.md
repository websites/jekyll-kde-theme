---
layout: page
title: Get Involved
konqi: /assets/img/konqi-dev.png
sorted: 5
---

## Get Involved!

Calligra Suite is a collection of free software application, made by a community of volunteers, with different skills, contributing to different areas.

No matter what your skills are, there is something you can do to help with the project. If you want to get in touch with the community, you can join the IRC channel: #calligra on irc.freenode.net, join the mailing-list [calligra-devel@kde.org](https://mail.kde.org/mailman/listinfo/calligra-devel), use our [community page](http://community.kde.org/Calligra), use the [forum]({{ site.forum }}), or finally [KDE’s bugs tracker](http://bugs.kde.org/).

## Build {{ site.title }} from Sources

The [community wiki](https://community.kde.org/Calligra/Building/3) provides resources
about setting up your development environment.

## Get in Touch!

Most development-related discussions take place on the [{{ site.email }} mailing list](http://mail.kde.org/mailman/listinfo/{{ site.email }})
Just join in, say hi and tell us what you would like to help us with!

## Coding

If you know C++ and Qt, start by reading the [First Contact](http://community.kde.org/Calligra/First_Contact) page on the Calligra Developers wiki where you wil find how to build the programs from source code, how to report bug or wish.

## Not a Programmer?

Not a problem! There's a plenty of other tasks that you can help us with to
make {{ site.title }} better even if you don't know any programming languages!

* [Bug triaging](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging) - help us find
  mis-filed, duplicated or invalid bug reports in Bugzilla
* [Localization](https://community.kde.org/Get_Involved/translation) - help to translate
  {{ site.title }} into your language
* [Documentation](https://community.kde.org/Get_Involved/documentation) - help us improve user
  documentation to make {{ site.title }} more friendly for newcomers
* [Promotion](https://community.kde.org/Get_Involved/promotion) - help us promote {{ site.title }}
  both online and offline
* [Updating wiki](https://userbase.kde.org/{{ site.title }}) - help updating the information present in
  the wiki, add new tutorials, ... - help us improve it to make it easier for others to join!
* Do you have any other idea? Get in touch!

