---
layout: component
name: Stage
screenshot: /assets/img/stage-summary.png
shortDescription: Create presentations containing a rich variety of elements
description: >
    Calligra Stage is a powerful and easy to use presentation application.You can dazzle your audience with stunning slides containing images, videos, animation and more
css-include: /css/component.css
redirect_from: 
- /stage/screenshots/
- /stage/template-contest/
---

Calligra Stage is an easy to use yet still flexible presentation application. You can easily create presentations containing a rich variety of elements, from graphics to text, from charts to images. Calligra Stage is extensible through a plugin system, so it is easy to add new effects, new content elements or even new ways of managing your presentation. Because of the integration with Calligra, all the power and flexibility of the Calligra content elements are available to Stage.

Calligra Stage natively uses the OpenDocument file format standard, for easy interchange with all ODF supporting applications which includes Microsoft Office. Some examples of easy-to-use features are support for layouts, a special slide overview view during presentations for the presenter, support for many different master sliders in
one presentation, cool transitions and a useful notes feature.

Calligra Stage is free software, developed by many developers from around the world. Everyone is welcome to help and try make Calligra Stage better: developers, documentation authors, template creation artist… And if you use Calligra Stage, please give us feedback and report [bugs](https://bugs.kde.org/). Join us on #calligra at [irc.freenode.net](irc.freenode.net), or on our [mailinglist](https://mail.kde.org/mailman/listinfo/calligra-devel).

## Screenshots


<center> Define multiple master slides and use them in your presentations. </center> <br/> 


<center>You can easily rearrange slides using the Slides Sorter View.</center> <br/>


<center>Use different layouts for your presentation.</center> <br/>


<center>Write notes for you slide. You can add all kind of shapes onto the notes page.</center> <br/>


<center>Stage supports most ODF slide transitions.</center> <br/>



